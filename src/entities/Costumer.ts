import {LocatedEntity} from "./LocatedEntity";
import {IdentifiedEntity} from "./IdentifiedEntity";

export class Costumer extends LocatedEntity implements IdentifiedEntity {

    constructor(public name: string, latLng: any) {
        super(latLng)
    }

    static FromJSON(data: any): Costumer {
        return new Costumer(data["name"], data["coords"])
    }

    getIdentity(): string {
        return this.name;
    }

}