export interface IdentifiedEntity {
    getIdentity(): string
}