import {LocatedEntity} from "./LocatedEntity";
import {IdentifiedEntity} from "./IdentifiedEntity";

export class Shop extends LocatedEntity implements IdentifiedEntity {
    constructor(public name: string, latLng: any) {
        super(latLng)
    }

    static FromJSON(data: any): Shop {
        return new Shop(data["name"], data["coords"])
    }

    getIdentity(): string {
        return this.name;
    }
}