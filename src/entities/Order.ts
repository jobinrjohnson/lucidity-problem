import {LocatedEntity} from "./LocatedEntity";
import {IdentifiedEntity} from "./IdentifiedEntity";
import {Shop} from "./Shop";
import {Costumer} from "./Costumer";

export class Order implements IdentifiedEntity {

    constructor(public name: string,
                public shop: Shop,
                public costumer: Costumer,
                public delay: number) {
    }

    getIdentity(): string {
        return this.name;
    }

}