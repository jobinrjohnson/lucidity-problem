import {LocatedEntity} from "./LocatedEntity";
import {IdentifiedEntity} from "./IdentifiedEntity";
import {Order} from "./Order";
import {EntityType, Pairs, PathFinder} from "../PathFinder";

export class DeliveryBoy extends LocatedEntity implements IdentifiedEntity {

    pendingOrders: Order[] = []

    constructor(public name: string, latLng: any) {
        super(latLng)
    }

    static FromJSON(data: any): DeliveryBoy {
        return new DeliveryBoy(data["name"], data["coords"])
    }

    getIdentity(): string {
        return this.name;
    }

    addOrder(order: Order) {
        this.pendingOrders.push(order)
    }

    findShortestPath(): IdentifiedEntity[] {
        if (this.pendingOrders.length === 0) {
            return []
        } else if (this.pendingOrders.length === 1) {
            const o = this.pendingOrders[0]
            return [this, o.shop, o.costumer]
        }

        // The real logic

        let entries: Pairs[] = [];
        this.pendingOrders.forEach(i => {
            entries.push(
                {entity: i.shop, type: EntityType.SHOP, delay: i.delay} as Pairs,
                {
                    depends: i.shop,
                    entity: i.costumer,
                    delay: 0,
                    type: EntityType.COSTUMER
                } as Pairs)
        })

        let p = new PathFinder(entries, this)
        return p.findPath()
    }

    processOrders() {
        let entities = this.findShortestPath()
        console.log("Best possible path")
        entities.forEach((e, i) => console.log(i, "=>", e.getIdentity()))
    }

}