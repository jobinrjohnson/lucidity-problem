import {DeliveryBoy} from "./DeliveryBoy";
import {Costumer} from "./Costumer";
import {Shop} from "./Shop";
import {getDistanceFromLatLonInKm} from "../utils";
import {IdentifiedEntity} from "./IdentifiedEntity";

export abstract class LocatedEntity implements IdentifiedEntity {
    protected lat: number
    protected lon: number

    protected constructor(latLng: any) {
        this.lat = parseFloat(latLng["lat"])
        this.lon = parseFloat(latLng["lon"])
    }

    getDistanceInKm(e: LocatedEntity): number {
        return getDistanceFromLatLonInKm(this.lat, this.lon, e.lat, e.lon)
    }

    getTravelDelay(e: LocatedEntity): number {
        let distance = this.getDistanceInKm(e)
        let availSpeed = 20
        return distance / availSpeed * 60
    }

    abstract getIdentity(): string

}