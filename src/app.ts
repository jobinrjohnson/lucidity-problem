import {DeliveryBoyPool} from "./pools/DeliveryBoyPool";
import {ShopPool} from "./pools/ShopPool";
import {CostumerPool} from "./pools/CostumerPool";
import {OrderHandler} from "./OrderHandler";

let deliveryBoyPool = new DeliveryBoyPool()
deliveryBoyPool.parseFromFile("data/delivery-boys.json")
// console.log(deliveryBoyPool)

let shopPool = new ShopPool()
shopPool.parseFromFile("data/shops.json")
// console.log(shopPool)

let costumerPool = new CostumerPool()
costumerPool.parseFromFile("data/costumers.json")
// console.log(costumerPool)

let oh = new OrderHandler(deliveryBoyPool, costumerPool, shopPool)
oh.parseFromFile("data/orders.json")

// console.log(deliveryBoyPool)
deliveryBoyPool.startOrderProcessing()

