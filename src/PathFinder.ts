import {Shop} from "./entities/Shop";
import {Costumer} from "./entities/Costumer";
import {IdentifiedEntity} from "./entities/IdentifiedEntity";
import {LocatedEntity} from "./entities/LocatedEntity";
import {DeliveryBoy} from "./entities/DeliveryBoy";

export enum EntityType {
    START,
    SHOP,
    COSTUMER
}

export type Pairs = {
    type: EntityType
    entity: Shop | Costumer | DeliveryBoy
    depends?: Shop
    delay: number
}

export class PathFinder {

    constructor(
        protected pairs: Pairs[],
        protected startingPoint: DeliveryBoy
    ) {
    }

    getAccessor(pair: Pairs): LocatedEntity {
        switch (pair.type) {
            case EntityType.COSTUMER:
                return pair.entity as Costumer;
            case EntityType.START:
                return pair.entity as DeliveryBoy
            case EntityType.SHOP:
                return pair.entity as Shop
        }
        throw new Error("Invalid type")
    }

    calcTotalDelay(selected: Pairs[]): number {
        let delay = 0
        let prevPair: Pairs | null = null
        for (const pair of selected) {
            if (prevPair === null) {
                prevPair = pair
                continue
            }
            delay += this.getAccessor(prevPair).getTravelDelay(this.getAccessor(pair))
            delay += pair.delay
            prevPair = pair
        }
        return delay
    }

    foundPaths: { path: Pairs[], delay: number }[] = []

    findPathRecursive(pairs: Pairs[], selected: Pairs[], distance: number) {
        if (pairs.length === 0) {
            this.foundPaths.push({path: selected, delay: this.calcTotalDelay(selected)})
            return;
        }

        let index = 0
        for (let pair of pairs) {

            if (pair.depends?.getIdentity()) {
                let dep = selected.find(i => i.entity.getIdentity() === pair.depends?.getIdentity())
                if (dep === undefined) {
                    index++
                    continue
                }
            }

            let newPath = [...selected, pair]
            let newPairs = [...pairs]
            newPairs.splice(index, 1)
            this.findPathRecursive(newPairs, newPath, distance + 1)
            index++
        }


    }

    findPath(): IdentifiedEntity[] {
        this.findPathRecursive(
            [...this.pairs],
            [{type: EntityType.START, entity: this.startingPoint, delay: 0}],
            0
        )
        if (this.foundPaths.length === 0) {
            return []
        }
        let min = this.foundPaths[0]
        for (let foundPath of this.foundPaths) {
            if (foundPath.delay < min.delay) {
                min = foundPath
            }
        }
        console.debug("debug:", "Delay and Found paths")
        this.foundPaths.forEach(x => {
            // @ts-ignore
            let p = x.path.map(m => (this.getAccessor(m) as IdentifiedEntity).getIdentity())
            console.debug(x.delay, p)
        })
        return min.path.map(p => p.entity)
    }

}