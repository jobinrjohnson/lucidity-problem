import {BasePool} from "./BasePool";
import {DeliveryBoy} from "../entities/DeliveryBoy";
import {Shop} from "../entities/Shop";

export class ShopPool extends BasePool<Shop> {
    protected parseFromRawData(data: any): Shop {
        return Shop.FromJSON(data);
    }
}