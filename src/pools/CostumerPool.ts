import {BasePool} from "./BasePool";
import {DeliveryBoy} from "../entities/DeliveryBoy";
import {Costumer} from "../entities/Costumer";

export class CostumerPool extends BasePool<Costumer> {
    protected parseFromRawData(data: any): Costumer {
        return Costumer.FromJSON(data);
    }
}