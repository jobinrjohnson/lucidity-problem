import {IdentifiedEntity} from "../entities/IdentifiedEntity";
import * as fs from "fs";

export abstract class BasePool<T extends IdentifiedEntity> {

    items: any = {}

    addItem(i: T) {
        this.items[i.getIdentity()] = i
    }

    getItemKeys(){
        return Object.keys(this.items)
    }

    protected abstract parseFromRawData(data: any): T;

    parseFromFile(filePath: string) {
        const rawData = fs.readFileSync(filePath)
        const data: any[] = JSON.parse(rawData.toString("utf-8"))
        data.forEach(x => {
            const i = this.parseFromRawData(x)
            this.addItem(i)
        })
    }

    findItem(identifier: string): T {
        if (this.items[identifier])
            return this.items[identifier]
        throw new Error("Invalid item requested")
    }

}