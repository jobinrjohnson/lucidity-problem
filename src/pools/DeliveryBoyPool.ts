import {BasePool} from "./BasePool";
import {DeliveryBoy} from "../entities/DeliveryBoy";

export class DeliveryBoyPool extends BasePool<DeliveryBoy> {
    protected parseFromRawData(data: any): DeliveryBoy {
        return DeliveryBoy.FromJSON(data);
    }

    startOrderProcessing() {
        for (let itemKey of this.getItemKeys()) {
            let deliveryBoy: DeliveryBoy = this.items[itemKey]
            deliveryBoy.processOrders()
        }
    }

}