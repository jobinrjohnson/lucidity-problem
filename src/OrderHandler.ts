import fs from "fs";
import {DeliveryBoyPool} from "./pools/DeliveryBoyPool";
import {CostumerPool} from "./pools/CostumerPool";
import {ShopPool} from "./pools/ShopPool";
import {Order} from "./entities/Order";

export class OrderHandler {

    constructor(
        protected deliveryBoyPool: DeliveryBoyPool,
        protected costumerPool: CostumerPool,
        protected restaurantPool: ShopPool) {
    }

    parseFromFile(filePath: string) {
        const rawData = fs.readFileSync(filePath)
        const data: any[] = JSON.parse(rawData.toString("utf-8"))
        data.forEach((x: {
            name: string,
            deliveryBoy: string,
            shop: string,
            costumer: string,
            shopDelay: number
        }) => {
            const shop = this.restaurantPool.findItem(x.shop)
            const costumer = this.costumerPool.findItem(x.costumer)
            const deliveryBoy = this.deliveryBoyPool.findItem(x.deliveryBoy)
            let order = new Order(
                x.name,
                shop,
                costumer,
                x.shopDelay
            )
            deliveryBoy.addOrder(order)
        })
    }
}