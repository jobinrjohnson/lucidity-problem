# Best Route Problem

## Why TypeScript

Since evaluation focuses on Modularity, Encapsulation, Readability and Extensibility.
Typescript supports a lot of features and design patterns. It's easier to write. Also I'm personally a great fan of TypeScript.

## Assumptions

* Name is unique across the system
* Existing assumptions provided in the question

## Concepts

### Pools and Entities

This is designed like a database like structure. Pool represents collections and entities represents item in each collection. Later pool class can be extended to use any kind of data provider like, databases, caches or file systems. 

### LocatedEntity

LocatedEntity is a abstract class, this can be extended by other classes which have location associated with it.  This can calculate distance between other LocatedEntity

### IdentifiedEntity

This is an interface. These are entities which have a unique identifier associated with it. Currently im using name as the identity

## File structure

```
- data                      Contains data
---- costomers.json         Contains costumer data
---- delivery-boys.json     Contains delivery boy information
---- orders.json            Contains order details
---- shops.json             Contains all restaurant information
- src                       Contains all source code
---- entities               Contains all entities
-------- LocatedEntity*     Is an abstract class which can be extended by others with location
-------- IdentifiedEntity*  Is an interface so that items can be assigned unique name if needed
-------- Costumer           Which is a Located(by coords), Identified(by name) entity
-------- Shop               Which is a Located(by coords), Identified(by name) entity
-------- DeliveryBoy        Which is a Located(by coords), Identified(by name) entity
-------- Order              Identified(by order name) entity
---- pools                  Contains Data pools, holding different entities
-------- BasePool           Abstract template pool class fo basic functionality
-------- .... Classes       extending BasePool
-------- ....
---- utils.ts               Utility functions
---- app.ts                 Main entry point
---- PathFinder             The main logic. This is seperated from rest so that this can be modified
---- OrderHandler           Parses the Order
```

## Scalability

* The app supports any number of orders, restaurants, costumers and delivery boys.
The solution uses bruteforce approach. I didn't find a better way to do that. 
Since the orders per delivery boy is limited (less than 10 max in real world scenarios)
The Performance won't be an issue.
* This can be exetended to add database/queueing/caching support to add/process orders, users, restaurants
* [This](https://gitlab.com/jobinrjohnson/lucidity-problem/-/blob/main/src/entities/LocatedEntity.ts#L16) function can be replaced with Google Route APIs to estimate and identity correct time delays. A cache can also be introduces here so that we can reduce the API calls.
* As of now the application maintains a local state. The [Pool class](https://gitlab.com/jobinrjohnson/lucidity-problem/-/blob/main/src/pools/BasePool.ts#L4) can be extended to integrate a database so that local state won't be there 
* The core logic is written in [This File](https://gitlab.com/jobinrjohnson/lucidity-problem/-/blob/main/src/PathFinder.ts#L20) Which is outside the system such that the logic can be modified without touching any other functionality

## How to run the project

* Run `yarn install` to install dependencies
* Run `yarn build` to build the project
* Add the required data to the JSON files in the `data` folder
* Run `yarn start` to start the execution

